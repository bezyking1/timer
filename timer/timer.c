#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

int main(int argc,char *argv[]) {
    if(argc != 3) {
       fprintf(stderr,"Usage: num m\n");
    }
     int x = atoi(argv[1]);
     for(int i = 0;i < (x*60);i++){
        usleep((1 * 1000000));
        if((i +1) % 60   == 0 ){
            printf("%dm\n",i/60);
        }
    }
     system("play /home/tux/Downloads/*?.mp3 &");
}
